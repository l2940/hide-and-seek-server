#include "player/role.hpp"

std::string roleToString(enum Role role) {
    switch (role)
    {
    case SEEKER:
        return "SEEKER";
        break;
    case HIDER:
        return "HIDER";
        break;
    default:
        return "UNKNOWN";
        break;
    }
}
