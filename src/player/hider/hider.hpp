#ifndef HIDE_AND_SEEK_HIDER_PLAYER_HPP
#define HIDE_AND_SEEK_HIDER_PLAYER_HPP

#include <string>
#include <vector>
#include <memory>
#include "utils/json.hpp"

#include "player/player.hpp"
#include "player/action.hpp"
#include "maze/block/block.hpp"

/**
 * @brief Abstract Hider class
 * 
 */
class Hider : public Player
{
private:
    std::string name;
    std::shared_ptr<Block> block;
    bool captured;

public:
    Hider(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {
        // by default...
        return actions.at(0);
    };
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {};
    virtual Role getRole() const;
    virtual nlohmann::json toJSON() const;

    void setCaptured(const bool &captured);
    const bool &isCaptured() const;

    const std::shared_ptr<Block> &getBlock() const;
    virtual const std::string toString() const;
    const bool hasBlock() const;
    void setBlock(const std::shared_ptr<Block> &block);

    ~Hider();
};

#endif