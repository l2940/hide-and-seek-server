#include "player/player.hpp"
#include "utils/randomGenerator.hpp"

/**
 * @brief Construct a new Player:: Player object
 * 
 * @param name 
 */
Player::Player(const std::string &name) : name(name) {
    
    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    
    std::vector<Orientation> orientations = PlayerAction::getOrientations();
    this->orientation = orientations[rGenerator->getRandomInt(orientations.size() - 1)];
}

/**
 * @brief Return the all the possible actions of the specific player
 * 
 * @return const std::vector<PlayerAction>& 
 */
const std::vector<PlayerAction> &Player::getPossibleActions() const {
    return this->possibleActions;
}

/**
 * @brief Return the name of the player
 * 
 * @return const std::string& 
 */
const std::string &Player::getName() const {
    return name;
}

/**
 * @brief Return the current location of the player
 * 
 * @return const Point& 
 */
const Point &Player::getLocation() const {
    return location;
}

/**
 * @brief Update the location of the player into the Maze
 * 
 * @param x 
 * @param y 
 */
void Player::setLocation(const unsigned int x, const unsigned int y) {
    location = Point(x, y);
}

/**
 * @brief Retrieve the current player orientation
 * 
 * @return Orientation 
 */
const Orientation Player::getOrientation() const {
    return orientation;
}

/**
 * @brief Update the orientation of the player
 * 
 * @param orient 
 */
void Player::setOrientation(Orientation orient) {
    this->orientation = orient;
}

/**
 * @brief return a JSOn representation of player
 * 
 * @return nlohmann::json 
 */
nlohmann::json Player::toJSON() const {

    nlohmann::json json;
    
    nlohmann::json json_role;

    json["role"] = getRole();
    json["roleStr"] = roleToString(getRole());
    json["name"] = this->getName();
    json["orientation"] = orientation;
    json["orientationStr"] = orientationToString(orientation);
    json["location"] = location.toJSON();

    return json;
}

/**
 * @brief Specific display of the player
 * 
 * @return const std::string 
 */
const std::string Player::toString() const {
    return "{role:" + roleToString(this->getRole()) + ", name: " + this->getName() + ", view: " + orientationToString(orientation) + "}";
}

/**
 * @brief Destroy the Player:: Player object
 * 
 */
Player::~Player() {}