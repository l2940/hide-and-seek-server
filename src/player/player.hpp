#ifndef HIDE_AND_SEEK_PLAYER_HPP
#define HIDE_AND_SEEK_PLAYER_HPP

#include <string>
#include <vector>
#include <memory>
#include <iostream>

#include "player/action.hpp"
#include "player/role.hpp"
//#include "maze/observation.hpp"
#include "maze/maze.hpp"
#include "utils/point.hpp"
#include "utils/json.hpp"

class Observation;

/**
 * @brief Abstract Player class
 * 
 */
class Player
{
protected:
    std::string name;
    std::vector<PlayerAction> possibleActions;
    Orientation orientation;
    Point location;

public:
    Player(const std::string &name);
    
    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const = 0;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) = 0;
    virtual Role getRole() const = 0;

    const std::vector<PlayerAction> &getPossibleActions() const;
    const std::string &getName() const;
    const Point &getLocation() const;
    void setLocation(const unsigned int x, const unsigned int y);

    const Orientation getOrientation() const;
    void setOrientation(Orientation orient);

    virtual const std::string toString() const;
    virtual nlohmann::json toJSON() const;
    
    ~Player();

    friend std::ostream &operator<<(std::ostream &os, const Player &p) { 
        return os << p.toString();
    }
};

// std::ostream &operator<<(std::ostream &os, const Player &p) 

#endif