#ifndef HIDE_AND_SEEK_BLOCK_GROUND_HPP
#define HIDE_AND_SEEK_BLOCK_GROUND_HPP

#include <string>

#include "maze/block/block.hpp"
#include "maze/block/kind.hpp"

class Ground : public Block
{
public:
    Ground() : Block() {};

    virtual BKind getKind() const {
        return BKind::GROUND;
    };

    virtual void interact() {
        // nothing happened with ground...
    };

    ~Ground() {};
};

#endif