#ifndef HIDE_AND_SEEK_BLOCK_UNDEFINED_HPP
#define HIDE_AND_SEEK_BLOCK_UNDEFINED_HPP

#include <string>

#include "maze/block/block.hpp"
#include "maze/block/kind.hpp"

/**
 * @brief Undefined block when Player want to see its observation
 * 
 */
class Undefined : public Block
{
public:
    Undefined() : Block() {};

    virtual BKind getKind() const {
        return BKind::UNDEFINED;
    };

    virtual void interact() {
        // nothing happened with ground...
    };

    ~Undefined() {};
};

#endif