#ifndef HIDE_AND_SEEK_BASIC_MAZE_HPP
#define HIDE_AND_SEEK_BASIC_MAZE_HPP

#include "maze/maze.hpp"

/**
 * @brief Example of Maze implementation
 * 
 */
class BasicMaze : public Maze
{

public:
    BasicMaze(unsigned int width, unsigned int height) : Maze(width, height) {};

    virtual void build();

    ~BasicMaze() {};
};

#endif