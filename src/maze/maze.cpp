#include "maze/maze.hpp"

#include <memory>

#include "game.hpp"
#include "player/hider/hider.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/undefined.hpp"
#include "utils/wuAntialiasing.hpp"

/**
 * @brief Construct a new Maze:: Maze object
 * 
 * @param width 
 * @param height 
 */
Maze::Maze(unsigned int width, unsigned int height) : width(width), height(height)
{

}


/**
 * @brief Method which add player if it's possible (ground cell with no player)
 * 
 * @param player 
 * @param x 
 * @param y 
 * @return true 
 * @return false 
 */
bool Maze::addPlayer(const std::shared_ptr<Player> player, unsigned int x, unsigned int y) {

    unsigned int cellIndex = (y * this->width) + x;

    // add player only on ground
    if (cells[cellIndex]->getBlock()->getKind() == BKind::GROUND) {

        // check also if another player is already present on that cell
        if (cells[cellIndex]->getPlayer() == nullptr) {

            // add cell and specify new player location
            cells[cellIndex]->setPlayer(player);
            player->setLocation(x, y);

            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * @brief Add player to an available random coordinate of the Maze
 * 
 * @param player 
 * @return true 
 * @return false 
 */
bool Maze::addPlayer(const std::shared_ptr<Player> player) {

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    // add player to Maze at random location until it's possible
    unsigned int x, y;
    do
    {
        // try random coordinate
        x = rGenerator->getRandomInt(this->width - 1);
        y = rGenerator->getRandomInt(this->height - 1);
    }
    while (!this->addPlayer(player, x, y));
    
    return true;
}

std::shared_ptr<Observation> Maze::doPlayerAction(const std::shared_ptr<Player> player, PlayerAction action) {

    // do the action player if possible and return the new state
    Point point = player->getLocation();
    unsigned int cellIndex = (point.y * this->width) + point.x;

    // retrieve the potential targeted cell index
    unsigned int targetCellIndex = cellIndex;

    if (action.orientation == Orientation::TOP) {
        targetCellIndex -= this->width;
    }
    if (action.orientation == Orientation::BOTTOM) {
        targetCellIndex += this->width;
    }
    if (action.orientation == Orientation::RIGHT) {
        targetCellIndex += 1;
    }
    if (action.orientation == Orientation::LEFT) {
        targetCellIndex -= 1;
    }

    // Move interaction
    if (action.interaction == Interaction::MOVE) {

        // player do not belong any longer to the current cell
        cells[cellIndex]->setPlayer(nullptr);

        // Check if an hider will be captured or not
        if (cells[targetCellIndex]->getPlayer() != nullptr && cells[targetCellIndex]->getPlayer()->getRole() == Role::HIDER) {

            if (player->getRole() == Role::SEEKER) {
                
                auto hider = std::dynamic_pointer_cast<Hider>(cells[targetCellIndex]->getPlayer());
                hider->setCaptured(true);
            }
        }
        // set the new player location
        // ToDo: check this part
        player->setLocation(targetCellIndex % this->width, int(targetCellIndex / this->width));
        
        cells[targetCellIndex]->setPlayer(player);
    } 
    else if (player->getRole() == Role::SEEKER && action.interaction == Interaction::HIT) {

        // Check if block is a Material block
        if (cells[targetCellIndex]->getBlock()->getKind() == BKind::MATERIAL) {

            // do the interaction with the block
            cells[targetCellIndex]->getBlock()->interact();

            // if block is no longer available (due to a lot of Player interact: hit..)
            // replace it by a ground block
            if (!cells[targetCellIndex]->getBlock()->isAvailable()) {

                std::shared_ptr<Block> block = std::make_shared<Ground>();
                cells[targetCellIndex]->setBlock(block);
            }
        }
    } else if (action.interaction == Interaction::CARRY || action.interaction == Interaction::PLACE){
        
        // get hider
        auto hider = std::dynamic_pointer_cast<Hider>(player);

        // std::cout << "Block kind found: " << blockKindToStringForJSON(cells[targetCellIndex]->getBlock()->getKind()) << std::endl;

        // place or carry move
        if (hider->hasBlock()) {
            
            // change the expected block into Material one
            std::shared_ptr<Block> block = hider->getBlock();
            cells[targetCellIndex]->setBlock(block);

            // hider do no longer has block
            hider->setBlock(nullptr);

        } else if (cells[targetCellIndex]->getBlock()->isMovable()) {
            
            // retrieve the block
            std::shared_ptr<Block> block = cells[targetCellIndex]->getBlock();
            hider->setBlock(block);

            // replace the block from Maze into Ground
            std::shared_ptr<Ground> groundBlock = std::make_shared<Ground>();
            cells[targetCellIndex]->setBlock(groundBlock);
        }
    }
    
    // always update the orientation of the player
    player->setOrientation(action.orientation);
    
    // return the new state
    auto observation = this->getObservationOfPlayer(player);

    return observation;
}

/**
 * @brief Add actions into current possible actions
 * 
 * @param actions 
 * @param cellIndex 
 * @param role 
 * @param move 
 * @param interact 
 */
void Maze::addActions(std::vector<PlayerAction> &actions, int cellIndex, std::shared_ptr<Player> player, Orientation orient) const {

    Role role = player->getRole();

    // check kind of block
    if (cells[cellIndex]->getBlock()->getKind() != BKind::BORDER) {
        
        // check if there another player on targeted cell
        // only seeker can catch hider
        // hider cannot pass on the same cell on an other hider
        // seeker cannot pass on the same cell on an other seeker
        if (cells[cellIndex]->getPlayer() != nullptr) {

            // Seeker can catch Hider (otherwise cannot move)
            if (role == Role::SEEKER && cells[cellIndex]->getPlayer()->getRole() == Role::HIDER)
                actions.push_back(PlayerAction(orient, Interaction::MOVE));
            
        } else {
            if (cells[cellIndex]->getBlock()->getKind() == BKind::GROUND) 
                actions.push_back(PlayerAction(orient, Interaction::MOVE));

            // check if hider can place a block
            if (cells[cellIndex]->getBlock()->getKind() == BKind::GROUND
                && role == Role::HIDER) {
                
                auto hider = std::dynamic_pointer_cast<Hider>(player);

                if (hider->hasBlock())
                    actions.push_back(PlayerAction(orient, Interaction::PLACE));
            }

            // check if seeker can interact with material
            if (cells[cellIndex]->getBlock()->getKind() == BKind::MATERIAL 
                && role == Role::SEEKER) {
                    actions.push_back(PlayerAction(orient, Interaction::HIT));
            }

            // check if hider can carry a block (block need to be movable)
            if (cells[cellIndex]->getBlock()->isMovable()
                && role == Role::HIDER) {

                auto hider = std::dynamic_pointer_cast<Hider>(player);

                if (!hider->hasBlock())
                    actions.push_back(PlayerAction(orient, Interaction::CARRY));
            }
        }
    }
}

std::vector<PlayerAction> Maze::getActionsOfPlayer(const std::shared_ptr<Player> player) const {
    
    std::vector<PlayerAction> currentActions;

    Point point = player->getLocation();
    unsigned int cellIndex = (point.y * this->width) + point.x;

    // player is currently on ground cell
    // player cannot go on border or on block
    // seeker can interact with material
    // hider cannot interact with material (for the moment)

    // Check each adjacent cells

    // TOP CELL
    int upperCell = cellIndex - this->width;

    // avoid out of bounds
    if (upperCell > 0) {
        this->addActions(currentActions, upperCell, player, Orientation::TOP);
    }

    // BOTTOM CELL
    int bottomCell = cellIndex + this->width;

    // avoid out of bounds
    if (bottomCell < int(this->width * this->height)) {
      this->addActions(currentActions, bottomCell, player, Orientation::BOTTOM);
    }

    // RIGHT CELL
    int rightCell = cellIndex + 1;

    // avoid out of bounds
    if (rightCell % int(this->width) != int(this->width)) {
      this->addActions(currentActions, rightCell, player, Orientation::RIGHT);
    }

    // LEFT CELL
    int leftCell = cellIndex - 1;

    // avoid out of bounds
    if (leftCell % int(this->width) != int((this->width - 1))) {
      this->addActions(currentActions, leftCell, player, Orientation::LEFT);
    }
    
    // By default player can just do nothing but at least change his orientation
    for (auto o : PlayerAction::getOrientations())
        currentActions.push_back(PlayerAction(o, Interaction::NOTHING));

    return currentActions;
}

std::shared_ptr<Observation> Maze::getObservationOfPlayer(const std::shared_ptr<Player> player) const {

    // build the expected observation view from player location and orientation
    std::vector<std::shared_ptr<MazeCell>> obsCells;

    Point loc = player->getLocation();
    Orientation orient = player->getOrientation();

    for (unsigned int y = 0; y < this->height; y++) {

        for (unsigned int x = 0; x < this->width; x++) {
                      
            // check Orientation of player (avoid cells behind player)
            Point currentLoc((int)x, (int)y);

            // check the player orientation
            bool canView = (orient == Orientation::TOP && loc.y >= (int)y) || (orient == Orientation::BOTTOM && loc.y <= (int)y) || 
                        (orient == Orientation::LEFT && loc.x >= (int)x) || (orient == Orientation::RIGHT && loc.x <= (int)x);

            if (isVisible(player, currentLoc) && canView) {
                // check block vision   
                unsigned int currentIndex = (currentLoc.y * this->width) + currentLoc.x;
                obsCells.push_back(cells[currentIndex]);
            }
            else {
                // not visible block
                std::shared_ptr<Block> block = std::make_shared<Undefined>();
                std::shared_ptr<MazeCell> currentCell = std::make_shared<MazeCell>(block);
                obsCells.push_back(currentCell);
            }
        }
    }
    
    return std::make_shared<Observation>(this->width, this->height, obsCells);
}

const std::shared_ptr<MazeCell> &Maze::getCell(unsigned int x, unsigned int y) const {
    
    unsigned int cellIndex = (y * this->width) + x;
    return cells[cellIndex];
}

const std::vector<std::shared_ptr<MazeCell>> &Maze::getCells() const {
    return cells;
}

bool Maze::isVisible(const std::shared_ptr<Player> &player, Point coordinate) const {

    Point loc = player->getLocation();

    Point p1(loc.x, loc.y);
    Point p2(coordinate.x, coordinate.y);

    // auto points = WuAlgo::getPoints(loc.x, loc.y, coordinate.x, coordinate.y);

    // std::cout << "From: (" << loc.x << ", " << loc.y << ") to (" << coordinate.x << ", " << coordinate.y << ")" << std::endl;
    // for (auto &p : points) {
    //     // std::cout << " -- " << p.x << ", " << p.y << std::endl;

    //     // if ((p.x != loc.x && p.y != loc.y) || (p.x != coordinate.x && p.y != coordinate.y)) {
    //         unsigned int currentIndex = (p.y * this->width) + p.x;
    //         auto block = cells[currentIndex]->getBlock();
    //         if (block->getKind() != BKind::GROUND) {
    //             return false;
    //         }
    //     // }
    // }

    // use of Bresenham's line generalized algorithm (plot line between the two coordinates)
    int dx = abs(p2.x - p1.x);
    int sx = p1.x < p2.x ? 1 : -1;
    int dy = -abs(p2.y - p1.y);
    int sy = p1.y < p2.y ? 1 : -1;
    int error = dx + dy;
    int e2;

    // std::cout << "From: (" << p1.x << ", " << p1.y << ") to (" << p2.x << ", " << p2.y << ")" << std::endl;
    while (true) {
        
        // std::cout << p1.x << ", " << p1.y << std::endl;
        unsigned int currentIndex = (p1.y * this->width) + p1.x;

        if (p1 == p2) {
            break;
        }

        // Point checkX(p1.x + sx, p1.y);

        // if (checkX != p2 && checkX.x >= 0 && checkX.y >= 0 && checkX.x < (int)this->width && checkX.y < (int)this->height) {
        //     unsigned int currentIndex = (checkX.y * this->width) + checkX.x;
        //     auto block = cells[currentIndex]->getBlock();
        //     if (block->getKind() != BKind::GROUND) {
        //         return false;
        //     }
        // }

        // Point checkY(p1.x, p1.y + sy);

        // if (checkY != p2 && checkY.x >= 0 && checkY.y >= 0 && checkY.x < (int)this->width && checkY.y < (int)this->height) {
        //     unsigned int currentIndex = (checkY.y * this->width) + checkY.x;
        //     auto block = cells[currentIndex]->getBlock();
        //     if (block->getKind() != BKind::GROUND) {
        //         return false;
        //     }
        // }

        auto block = cells[currentIndex]->getBlock();
        if (block->getKind() != BKind::GROUND) {
            return false;
        }

        e2 = 2 * error;
        if (e2 >= dy) {
            if (p1.x == p2.x) {
                break;
            } 
            error = error + dy;
            p1.x = p1.x + sx;
        }

        if (e2 <= dx) {
            if (p1.y == p2.y) {
                break;
            } 
            error = error + dx;
            p1.y = p1.y + sy;
        }
    }

    return true;
}

bool Maze::setCells(std::vector<std::shared_ptr<MazeCell>> cells) {

    if (cells.size() == width * height) {
        this->cells = cells;
        return true;
    }
    return false;
}

/**
 * @brief JSON representation of the whole Maze
 * 
 * @return const nlohmann::json 
 */
const nlohmann::json Maze::toJSON() const {
    nlohmann::json maze;

    maze["height"] = this->height;
    maze["width"] = this->width;
    
    nlohmann::json json_cells;

    for (auto c : cells) {
        
        nlohmann::json current_cell;

        // display player or current block
        current_cell["block"] = c->getBlock()->toJSON();

        if (c->hasPlayer())
            current_cell["player"] = c->getPlayer()->toJSON();
        
        json_cells.push_back(current_cell);
    }

    maze["cells"] = json_cells;

    return maze;
}

/**
 * @brief Display the current block Maze
 * 
 * @return std::string 
 */
std::string Maze::toString() const {
    std::string mazeStr = "";

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {
            
            unsigned int cellIndex = (i * this->width) + j;

            if (cells[cellIndex]->getPlayer() != nullptr)
                mazeStr += cells[cellIndex]->getPlayer()->getName() + " ";
            else    
                mazeStr += blockKindToString(cells[cellIndex]->getBlock()->getKind()) + "  ";
        }

        mazeStr += "\n";
    }   

    return mazeStr;
}

/**
 * @brief Destroy the Maze:: Maze object
 * 
 */
Maze::~Maze(){}
