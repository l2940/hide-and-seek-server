#include "utils/graph.hpp"

Graph::Graph(const std::shared_ptr<Maze> &maze)
{
    // loop over Maze and add egdes
    for (unsigned int i = 0; i < maze->getHeight(); i++) {
        for (unsigned int j = 0; j < maze->getWidth(); j++) {
            
			// w, h => j, i
            auto cell = maze->getCell(j, i);

			// check adjacent cells (keep only material and ground cells)
			if (i > 0) {
				auto topCell = maze->getCell(j, i - 1);
				auto block = topCell->getBlock();

				if (block->getKind() != BKind::BORDER && block->getKind() != BKind::UNDEFINED)
					addEdge(cell, topCell);
			}

			if (i < maze->getHeight() - 1) {
				auto bottomCell = maze->getCell(j, i + 1);
				auto block = bottomCell->getBlock();

				if (block->getKind() != BKind::BORDER && block->getKind() != BKind::UNDEFINED)
					addEdge(cell, bottomCell);
			}

			if (j > 0) {
				auto leftCell = maze->getCell(j - 1, i);
				auto block = leftCell->getBlock();

				if (block->getKind() != BKind::BORDER && block->getKind() != BKind::UNDEFINED)
					addEdge(cell, leftCell);
			}

			if (j < maze->getWidth() - 1) {
				auto rightCell = maze->getCell(j + 1, i);
				auto block = rightCell->getBlock();

				if (block->getKind() != BKind::BORDER && block->getKind() != BKind::UNDEFINED)
					addEdge(cell, rightCell);
			}
        }
    }   
}

void Graph::addEdge(const std::shared_ptr<MazeCell> &v, const std::shared_ptr<MazeCell> &w)
{
	adj[v].push_back(w); // Add w to v’s list.
}

bool Graph::findPath(const std::shared_ptr<MazeCell> &from, const std::shared_ptr<MazeCell> &to)
{
	// Mark all the vertices as not visited
	std::map<std::shared_ptr<MazeCell>, bool> visited;
	
	for (auto &p : adj) {
		visited[p.first] = false;
	}

	// Create a queue for BFS
	std::list<std::shared_ptr<MazeCell>> queue;
	std::shared_ptr<MazeCell> current = from;

	// Mark the current node as visited and enqueue it
	visited[current] = true;
	queue.push_back(current);

	while(!queue.empty())
	{
		// Dequeue a vertex from queue and print it
		current = queue.front();
		queue.pop_front();

		for (auto adjecent: adj[current])
		{
			if (!visited[adjecent])
			{
                // Node found, then end (path exist)
                if (adjecent == to)
                    return true;

				visited[adjecent] = true;
				queue.push_back(adjecent);
			}
		}
	}

    return false;
}