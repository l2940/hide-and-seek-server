#ifndef HIDE_AND_SEEK_HELPER_HPP
#define HIDE_AND_SEEK_HELPER_HPP

class Game;

class Helper {

public:
    static bool isValid(const Game &game);
};

#endif // HIDE_AND_SEEK_HELPER_HPP
