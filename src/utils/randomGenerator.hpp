#ifndef HIDE_AND_SEEK_UTILS_RAND_GENERATOR_HPP
#define HIDE_AND_SEEK_UTILS_RAND_GENERATOR_HPP

#include <random>
#include <memory>

constexpr int DOUBLE_MIN = 0;
constexpr int DOUBLE_MAX = 1;

/**
 * @brief Singleton for number double or integer generation
 * 
 */
class RandGenerator
{
    private:

        // usefull random device and engines
        std::random_device rd;
        std::default_random_engine eng;
        std::uniform_real_distribution<double> uniDouble;

        static std::shared_ptr<RandGenerator> instance;
        RandGenerator();

    public:

        /**
         * @brief Get the Singleton Instance object
         * 
         * @return std::shared_ptr<RandGenerator> 
         */
        static const std::shared_ptr<RandGenerator> &getInstance();

        /**
         * @brief Get the Probability between 0 and 1
         * 
         * @return double 
         */
        double getProbability();

        /**
         * @brief Get a random number bewteen a specific interval
         * 
         * @param min 
         * @param max 
         * @return int 
         */
        int getRandomIntInterval(int min, int max);


        /**
         * @brief Get a random number bewteen a specific max value [0, max]
         * 
         * @param max 
         * @return int 
         */
        int getRandomInt(int max);
};


#endif