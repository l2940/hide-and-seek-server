#include <iostream>

#include "rest_server.hpp"

struct sigaction old_action;

namespace ServerContext {

    int serverPort = 8080;
    std::string serverAddress = "0.0.0.0";
    GameServer server;
};

int main(int argc, char* argv[])
{

    if (argc >= 2)
    {
        ServerContext::serverPort = static_cast<uint16_t>(std::stol(argv[1]));
    }

    if (argc >= 3)
    {
        ServerContext::serverAddress = argv[2];
    }

    ServerContext::server.init();

    std::cout << "Server is launched..." << std::endl;

    // Catch end of program
    struct Handler {
        static void disconnect(int sig_no) {

            ServerContext::server.stop();

            sigaction(SIGINT, &old_action, NULL);
            kill(0, SIGINT);
        }
    };

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = &Handler::disconnect;
    sigaction(SIGINT, &action, &old_action);
    sigaction(SIGKILL, &action, &old_action);
    sigaction(SIGTERM, &action, &old_action);
    sigaction(SIGABRT, &action, &old_action);

    ServerContext::server.start(ServerContext::serverPort, ServerContext::serverAddress);
}